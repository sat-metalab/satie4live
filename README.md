# SATIE4Live

SATIE4Live is a Max4Live device connecting [SATIE](https://gitlab.com/sat-metalab/satie) with Ableton Live.  
[JACK](https://jackaudio.org/) is required.

# SETUP

Place [SATIE4Live.amxd](/M4L/SATIE4Live.amxd) in your Max4Live device folder.

For complete usage instruction please refer to [OSC and Audio S4L Jack](/OSC%20and%20Audio%20S4L%20Jack.md).

For instructions on how to install JACK on windows, read [JACK Installation](/JACK%20installation.md).

The configuration for use in a dome is [HERE](/Dome%20config%20S4L.md).