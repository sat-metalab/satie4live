# Dome configuration / Live + SATIE with Jack + S4L


We need to make a small adjustment to pass from stereo to dome configuration.

<p>&nbsp;</p> 

## 1. SATIE for Dome

In the configuration of SATIE, the listening format must be changed to `domeVBAP` :
```
(
Server.default.options.inDevice_("JackRouter");
Server.default.options.outDevice_("JackRouter");
s.options.sampleRate = 44100;
~config = SatieConfiguration(s,
  listeningFormat: [\domeVBAP],
  outBusIndex: [0]
);
~config.server.options.numInputBusChannels = 32;
~satie = Satie(~config).boot();
)
```

That is all there is to do on SATIE's side.

<p>&nbsp;</p> 

## 2. JACK for Dome

*The following setp is sepcific to the Satosphère.*

1. Open Jack and open the Setup window.
2. In the Interface drop-down menu, select ASIO::RME MADIFACE
3. Check you buffer size in the Frames/Period drop-down menu, something that might be running at 16 on your computer might pop a lot in the Dome (512 is good).
4. In the Advanced window, check your number of inputs and outputs. By default, there may be less than you need.
5. Now to connect your outputs, the operation is the same as when doing it in stereo, but the system now shows the number of playback ports as specified in the Advanced window.

