# Installing Jack on Windows x64

1. Go to https://jackaudio.org/downloads/ and download : JACK 1.9.20 win64.
2. Execute : jack2-win64-v1.9.20.exe
3. At the 'Select Component' step, choose "Full Installation (with JACK-Router)" or tick the "JACK-Router ASIO Driver" box
4. Follow the remaining steps and install
5. To open JACK, execute "QjackCtl" in the Start Menu

## Setting up the number of inputs/outputs

1. Go to C:\Program Files\JACK2\jack-router\win64
2. Open "JackRouter.ini"
 - To do so : open Command Prompt (cmd.exe) in Administrator Mode (that way you will be allowed to make modifications and save the file)
 - Write ```cd c:/Program Files/JACK2/jack-router/win64``` (the default installation path, if you installed it somewhere else just type the new path) and press Enter
 - Then write ```JackRouter.ini``` and press Enter, it will open the file
3. Type in desired numbers and save it (overwrite) 

 ![image](media/screens/JACK_Config_i.o.png "JACK Config I/O")


## Selecting your interface/soundcard

1. In the JACK window, click "Setup"
2. In the "Interface" drop-down menu, select "ASIO::ASIO4ALL v2" if you're using your computer internal soundcard  
Or select ASIO::YourExternalCardName if you're using an external soundcard
3. Other parameters do not need to be modified, apparently

![image](media/screens/JACK_Config_ASIO.png "JACK Config ASIO")

## Launching JACK

1. To start JACK, simply press the "Play" button of the Main window
2. Sometimes, JACK does not start correctly, and it shows crash report windows with an error message
3. If it happens just closing these windows and clicking on the "Play" button a second time makes it work properly

## Routing with JACK

1. Any audio application can be connected to JACK if it can use ASIO, you would just need to set JACK as the driver of your application
2. Opening the "graph" window allows you to see every application that JACK is managing
3. Simply drag and connect cables from outputs to inputs to change the signal flow
4. Just make sure that your final outputs are always routed to the playback inputs of the "system" block, representing your speakers/headphones

![image](media/screens/JACK_Graph_routing.png "JACK Graph routing")
