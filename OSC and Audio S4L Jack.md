# TUTORIAL :  
## SATIE4Live (S4L) : Sending OSC and audio from Live into SATIE/SuperCollider, using Jack

##### _ASIO4ALL is required_  
##### _All tests done in Windows 10 x64_
Jack : https://jackaudio.org/downloads/

### SETUP :

 1. Open Jack (QjackCtl)
   - In Setup/Settings set Interface to ASIO::ASIO4ALL v2 (for onboard soundcard) or ASIO::YourExternalSoundcard 
   - Sample rate must be the same between all apps
   - Frames/Period is a buffer time
 2. Open SuperCollider
  - Set jack as your audio device : ```s.options.device_("JackRouter");```
  - Set sample rate to desired value : ```s.options.sampleRate = 44100;```
  - Boot server
  - __To use with SATIE, execute the code below__ : 
  ``` 
(  
Server.default.options.inDevice_("JackRouter");    
Server.default.options.outDevice_("JackRouter");  
~config = SatieConfiguration(s,  
    listeningFormat: [\stereoListener],  
    outBusIndex: [0]   // zero based offset  
);  
~config.server.options.numInputBusChannels = 16;
~satie = Satie(~config).boot();  
)
```

![image](media/screens/SATIE_Config_for_JACK.png "SATIE config for Live")
 3. Open Ableton Live
  - Set ASIO as your driver and Jack as your audio device : Options/Preferences/Audio/Driver : ASIO, Audio Device : JackRouter 

  ![image](media/screens/Live_preferences_for_JACK_cropped.png "Live preferences for JACK")  

   - Open your output config. and make sure every output is activated (yellow), depending on what you're doing you might want to select only mono outputs (for spatialization), the number displayed is the [number found in "JackRouter.ini"](#add-more-inputs-and-outputs-to-applications-in-jack)

  ![image](media/screens/Live_outputs_Config_cropped.png "Live outputs config")


### CONNECT

 1. In Jack, open the graph window, you should see Ableton Live and SuperCollider (as "scsynth"). By default, their inputs are connected to system's "capture 1&2" outputs, and their outputs are connected to system's "playback 1&2" inputs.
 
 2. Disable Live's outputs (Click + Del).

 3. Drag new cables from Live's outputs to scsynth's inputs by maintaining left click. The signal path should be "Capture => Live => SCSynth => Playback".
 
### SEND & RECEIVE

 1. In Live you can send any track you want into SuperCollider/SATIE.
  - Open the in/out section of your channels : click  on the i·o button at the bottom-right (should turn yellow).
  - In the i·o section of the desired track, click on the drop-down menu "master" and select "Ext.Out". Then right below it, select on which channel you want to send it.   
 
 2. To use with SATIE simply load SATIE4Live on your track. Select the input with the drop-down menu, then on S4L choose the same input number as the output of your track (so if you choose output 4 in Live, select input 4 in S4L).

   ![image](media/screens/LIVE_track_outputs_Routing.png "Live track outputs routing")

### ADD more inputs and outputs to applications in JACK :

 * Execute Command Prompt in Administrator mode
 * Move to your Jack directory `cd C:\Program Files\JACK2\jack-router\win64`
 * Open "JackRouter.ini" (should be opened by an admin, that's why we use cmd)
 * There you should see inputs and outputs number displayed
 * Change them to desired values
 * Save the file

 ### Using S4L :

 You can now control SATIE's spatialization parameters using the SATIE4Live device.   
 Set the parameters manually, draw automations, or connect a MIDI interface and assign knobs with parameters.  
 For now S4L has 6 parameters :
  * aziDeg = Azimuth Degree, position of the sound on the horizontal plane from -180 to 180. 0 is front, -180 and 180 are rear, 90 and -90 are sides.
  * eleDeg = Elevation Degree, position of the sound on the vertical plane.
  * gainDB = Gain in Decibels, allow to play with the distance of the sound (loud = close, quiet = far).
  * Delay
  * Lowpass
  * Spread = Spread the signal between adjacent loudspeakers so it's less focused on a point in space, therefore wider.


 


